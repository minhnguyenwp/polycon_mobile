(function( $ ) {
/**
 * START - ONLOAD - JS
 * 1. HOT NEWS
 * 2. MAIN SLIDER
 * 3. SHOW CONTENT BLOCK CONSULT
 * 4. CLIENTS SLIDER
 * 5. MENU MOBILE
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */
/**
 * 1. HOT NEWS
 */
function hotNews() {
    if(!$('.hotnews').length) { return;}

    $('.hotnews').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 300,
        centerMode: false,
        variableWidth: false,
        arrows: false,
        dots: false,
        fade: true,
    });
}

/**
 * 2. MAIN SLIDER
 */
function mainSlider() {
    if(!$('.main-slider').length) { return;}

    $('.main-slider').slick({
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 300,
        centerMode: false,
        variableWidth: false,
        arrows: false,
        dots: true
    });
}
/**
 * 3. SHOW CONTENT BLOCK CONSULT
 */
function showContentConsult() {
    if(!$('.mod-content.mod-consult').length) { return; }

    $('.mod-content.mod-consult .mod-title').on('click', function(e) {
        e.preventDefault();

        var $title = $(this),
            $blk   = $title.closest('.mod-content.mod-consult');
        
        if($title.hasClass('act')) {
            $title.removeClass('act');
            $blk.removeClass('shw');
        } else {
            $title.addClass('act');
            $blk.addClass('shw');
        }
    });
}
/**
 * 4. CLIENTS SLIDER
 */
function clientsSlider (objClients) {
    if(!$(objClients).length) { return; }

    $(objClients).slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        variableWidth: true,
        arrows: false,
        dots: false,
    });
}
/**
 * 5. MENU MOBILE
 */

// 5.1 Show Menu Mobile
function menuMobile() {
    if(!$('.menu_mb').length) { return; }

    $('.menu_mb').on('click', function(e) {
        var $a_menu =   $(this),
            $icon   =   $a_menu.find('.fa'),
            $menu   =   $a_menu.siblings('.header-menu'),
            $body   =   $a_menu.closest('body');
        
        if($a_menu.hasClass('act')) {
            $a_menu.removeClass('act');
            $menu.removeClass('shw');
            $body.removeClass('mn-open');

            // change icon
            $icon.addClass('fa-bars');
            $icon.removeClass('fa-times');
        } else {
            $a_menu.addClass('act');
            $menu.addClass('shw');
            $body.addClass('mn-open');

            // change icon
            $icon.removeClass('fa-bars');
            $icon.addClass('fa-times');
        }
    });
}

// 5.2 append html for menu mobile
function appendHtmlMenuMB() {
    if(!$('.header-menu').length) { return; }

    $('.header-menu li').each(function(e) {
        if($(this).hasClass('has-child')) {
            $(this).append('<span class="shw_menu"><i class="fa fa-plus"></i><span>')
        }
    });

    // 5.3 show sub-menu
    showSubMenuMB();
}
// 5.3 show sub-menu
function showSubMenuMB() {
    if(!$('.shw_menu').length) { return; }

    $('.shw_menu').on('click', function(e) {
        e.preventDefault();

        var $a_click  =     $(this),
            $sub_menu =     $a_click.siblings('.dropdown-menu'),
            $li       =     $a_click.closest('.has-child').siblings('.has-child'),
            $icon     =     $a_click.find('.fa');

        if($a_click.hasClass('act')) {
            $a_click.removeClass('act');
            $sub_menu.removeClass('shw');

            // change icon
            $icon.addClass('fa-plus');
            $icon.removeClass('fa-minus');
        } else {
            $a_click.addClass('act');
            $sub_menu.addClass('shw');

            // close menu siblings
            $li.find('.shw_menu').removeClass('act');
            $li.find('.dropdown-menu').removeClass('shw');
            $li.find('.fa').addClass('fa-plus');
            $li.find('.fa').removeClass('fa-minus');;

            // change icon
            $icon.removeClass('fa-plus');
            $icon.addClass('fa-minus');
        }
    });
}
/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    // 1.
    hotNews();

    // 2. 
    mainSlider();

    // 3. 
    showContentConsult();

    // 4. 
    clientsSlider('.mod-clients ul');
    clientsSlider('.project-related');

    // 5.1 
    menuMobile();

    // 5.2
    appendHtmlMenuMB();
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;
})(jQuery);
